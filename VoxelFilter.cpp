// VoxelFilter.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "VoxelFilter.h"
#include "LasFileIO.h"

int _tmain(int argc, _TCHAR* argv[])
{
	std::cout << "请输入抽稀点云\n";
	std::string file_name;
	std::cin >> file_name;
	Eigen::Vector4d offset, min_p, max_p;
	pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = readLasFile(file_name, offset, min_p, max_p);

	clock_t t1 = clock();
	
	clock_t t2 = clock();
	std::cout << "抽稀耗时:" << double(t2 - t1) / CLOCKS_PER_SEC << "s\n";

	writeLasFile(result_cloud, "simplified.las", offset, min_p, max_p);

	return 0;
}

