#include "featureCalculation.h"

#include <opencv2\core\types_c.h>
#include <opencv2\core\core_c.h>

#include <pcl\kdtree\kdtree_flann.h>

#include <concurrent_vector.h>
#include <ppl.h>

using namespace std;
using namespace concurrency;

bool featureCalculation::calculateEigenFeature(pointCloudXYZI &cloud, std::vector<int> &searchIndices, eigenFeature &ptFeature)
{
	if (searchIndices.size() < 5)
		return false;

	CvMat* pData = cvCreateMat(searchIndices.size(), 3, CV_32FC1);
	CvMat* pMean = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVals = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVecs = cvCreateMat(3, 3, CV_32FC1);

	for (size_t i = 0; i < searchIndices.size(); ++i)
	{
		cvmSet(pData, i, 0, cloud.points[searchIndices[i]].x);
		cvmSet(pData, i, 1, cloud.points[searchIndices[i]].y);
		cvmSet(pData, i, 2, cloud.points[searchIndices[i]].z);
	}
	cvCalcPCA(pData, pMean, pEigVals, pEigVecs, CV_PCA_DATA_AS_ROW);

	//eigne vectors;
	ptFeature.normalDirection.x() = cvmGet(pEigVecs, 2, 0);
	ptFeature.normalDirection.y() = cvmGet(pEigVecs, 2, 1);
	ptFeature.normalDirection.z() = cvmGet(pEigVecs, 2, 2);

	ptFeature.principleDirection.x() = cvmGet(pEigVecs, 0, 0);
	ptFeature.principleDirection.y() = cvmGet(pEigVecs, 0, 1);
	ptFeature.principleDirection.z() = cvmGet(pEigVecs, 0, 2);

	//eigne values;
	ptFeature.lamada1 = cvmGet(pEigVals, 0, 0);
	ptFeature.lamada2 = cvmGet(pEigVals, 0, 1);
	ptFeature.lamada3 = cvmGet(pEigVals, 0, 2);

	//curvature;
	if (ptFeature.lamada1 + ptFeature.lamada2 + ptFeature.lamada3 == 0)
	{
		ptFeature.curvature = 0.333;
	}

	else
	{
		ptFeature.curvature = ptFeature.lamada3 / (ptFeature.lamada1 + ptFeature.lamada2 + ptFeature.lamada3);

	}


	//distance from original to plane;
	double a, b, c, d;
	double x0, y0, z0;
	a = ptFeature.normalDirection.x();
	b = ptFeature.normalDirection.y();
	c = ptFeature.normalDirection.z();
	x0 = cvmGet(pMean, 0, 0);
	y0 = cvmGet(pMean, 0, 1);
	z0 = cvmGet(pMean, 0, 2);
	ptFeature.dis = -(a*x0 + b*y0 + c*z0);

	cvReleaseMat(&pEigVecs);
	cvReleaseMat(&pEigVals);
	cvReleaseMat(&pMean);
	cvReleaseMat(&pData);

	return true;
}

bool featureCalculation::calculateEigenFeaturesBasedOnRadiusSearch(pointCloudXYZI &cloud, float radius, concurrent_vector<eigenFeature> &ptFeatures)
{
	/*����KD���Լ��洢������ź;��������*/
	pcl::KdTreeFLANN<pcl::PointXYZI>kdtree;
	
	kdtree.setInputCloud(cloud.makeShared());

	concurrency::parallel_for(size_t(0), cloud.size(), [&](size_t i)
	{
		vector <int> searchIndices;
		vector <float> pointDistance;
		searchIndices.clear();
		pointDistance.clear();
		int neighborNum = kdtree.radiusSearch(cloud.points[i], radius, searchIndices, pointDistance);
		calculateEigenFeature(cloud, searchIndices,ptFeatures[i]);
		ptFeatures[i].pt = cloud.points[i];
		ptFeatures[i].ptIndex = i;
	});
	return true;
}

bool featureCalculation::calculateEigenFeaturesBasedOnNearestKSearch(pointCloudXYZI &cloud, unsigned short ptNum, concurrency::concurrent_vector<eigenFeature> &ptFeatures)
{
	/*����KD���Լ��洢������ź;��������*/
	pcl::KdTreeFLANN<pcl::PointXYZI>kdtree;

	kdtree.setInputCloud(cloud.makeShared());

	concurrency::parallel_for(size_t(0), cloud.size(), [&](size_t i)
	{
		vector <int> searchIndices;
		vector <float> pointDistance;
		searchIndices.clear();
		pointDistance.clear();
		int neighborNum = kdtree.nearestKSearch(cloud.points[i], ptNum, searchIndices, pointDistance);
		calculateEigenFeature(cloud, searchIndices, ptFeatures[i]);
		ptFeatures[i].pt = cloud.points[i];
		ptFeatures[i].ptIndex = i;
	});

	return true;
}

