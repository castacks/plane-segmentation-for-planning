#ifndef PLANESEGMENTATION
#define PLANESEGMENTATION

#include "dataIo.h"
#include "featureCalculation.h"

#include <pcl\PointIndices.h>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/algorithm.h>
#include <CGAL/Cartesian.h>

typedef CGAL::Simple_cartesian<double>       Kernel;
typedef Kernel::Point_2                      Point_2;
typedef Kernel::Point_3                      Point_3;
typedef Kernel::Plane_3                      Plane_3;

class planeSegmentation:public dataIo, public featureCalculation
{
public:

	struct planeBoxBound3d
	{
		pcl::PointXYZ pt[4];
	};

	struct planeBoxBound2d
	{
		pcl::PointXY pt[4];
	};

	struct planeFunction
	{
		// function: ax + by + cz + d = 0;
		float a;
		float b;
		float c;
		double d;
		planeFunction()
		{
			a = b = c = 0.0f;
			d = 0.0;
		}
	};

	struct planeFeature
	{
		planeFunction fun;
		planeBoxBound3d bound;
	};

	//plane segmentation;
	size_t planeSegmentationBasedOnRegionGrowingRadius(pointCloudXYZI &pointCloud, float radiusFeature,
	                                                   float includedAngle, float Pt2PlaneDis, float searchRadius,
													   std::vector <pcl::PointIndices> &planes);
	

	void calculateTheFeatureOfPlanes(pointCloudXYZI &pointCloud, const std::vector<pcl::PointIndices> &planesIndices,
		                             std::vector<planeFeature> &planesFeatures);

	void calculateTheFunctionOfPlane(pointCloudXYZI &planePointCloud, planeFunction &planeFun);


	void calculateTheBoundOfPlane(pointCloudXYZI &planePointCloud, planeFunction planeFun, planeBoxBound3d &planeBound);

	void calculateProjectionPointCloud(pointCloudXYZI &planePointCloud, planeFunction planeFun, pointCloudXYZ  &projectionPointCloud);

	void Projecion3DPlaneTo2D(planeFunction planeFun, pointCloudXYZ &projectionPointCloud3d, pointCloudXY &projectionPointCloud2d);

	void computePrincipleDirectionOfPointCloud2d(pointCloudXY &projectionPointCloud2d, Eigen::Vector2f & principleDirection);
	
	void transformPointCloudXaxisToPrincipleDirection(pointCloudXY &projectionPointCloud2d, Eigen::Vector2f  principleDirection,
		                                              pointCloudXY &transformPointCloud2d);

	void calculateBoundInPcaCoordinate(pointCloudXY &transformPointCloud2d, planeBoxBound2d &bound2d);

	void InverseTransformToOriginalCoordinate(const planeBoxBound2d bound2dInPCA, Eigen::Vector2f  principleDirection, planeBoxBound2d &bound2dInOri);

	void Projecion2DPointTo3D(planeBoxBound2d bound2d, planeFunction planeFun, planeBoxBound3d &bound3d);

	

protected:


private:
	float ComputeIncludedAngleBetweenVector(float vx1, float vy1, float vz1, float vx2, float vy2, float vz2)
	{
		/*计算邻域点和种子点法向量的夹角;*/
		float n_n1 = vx1*vx2 + vy1*vy2 + vz1*vz2;
		float n_n = sqrt(vx1*vx1 + vy1*vy1 + vz1*vz1);
		float n1_n1 = sqrt(vx2*vx2 + vy2*vy2 + vz2*vz2);
		float Cosnormal = abs(n_n1 / (n_n*n1_n1));
		return Cosnormal;
	}

	/*a_point是平面外一点,a,b,c,d是平面方程的系数;*/
	float ComputeDistanceFromPointToPlane(pcl::PointXYZI & a_point, float a, float b, float c, float d)
	{
		/*计算邻域点到种子点所在平面的垂直距离;*/
		float x1, y1, z1;
		//平面外一点（x1,y1,z1）;
		x1 = a_point.x; y1 = a_point.y; z1 = a_point.z;
		float dis;
		double g = sqrt(a*a + b*b + c*c);//求平面方程系数nx,ny,nz的平方和的开平方;
		double f1 = a*x1;
		double f2 = b*y1;
		double f3 = c*z1;
		double f4 = d;
		double f = abs(f1 + f2 + f3 + f4);
		dis = (f / g);
		return dis;
	}

	void outputPlanes(const std::string &fileName, pointCloudXYZI &pointCloud, const std::vector <pcl::PointIndices> &planes);

	void displayPlanesAndBounds(pointCloudXYZI &pointCloud, const std::vector<pcl::PointIndices> &planesIndices,
		                        std::vector<planeFeature> &planesFeatures);

	void displayPlaneAndBound(pointCloudXYZI &pointCloud, planeFeature  planeFea);

};

#endif