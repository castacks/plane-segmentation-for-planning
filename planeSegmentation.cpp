#include "planeSegmentation.h"
#include "featureCalculation.h"
#include "dataIo.h"


#include <set>
#include <list>
#include <deque>
#include <fstream>
#include <vector>

#include <pcl\kdtree\kdtree_flann.h>

#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/extract_indices.h>

#include <liblas/liblas.hpp>
#include <liblas/version.hpp>
#include <liblas/point.hpp>

#include <opencv2\core\types_c.h>
#include <opencv2\core\core_c.h>

#include <CGAL\Plane_3.h>


using namespace  std;
using namespace concurrency;

bool CmpPointCurvatureS(featureCalculation::eigenFeature &a, featureCalculation::eigenFeature &b)
{
	if (a.curvature<b.curvature)
	{
		return true;
	}
	else
	{
		return false;
	}
}

size_t planeSegmentation::planeSegmentationBasedOnRegionGrowingRadius(pointCloudXYZI &pointCloud, float radiusFeature, float includedAngle,
	                                                                  float Pt2PlaneDis, float searchRadius, std::vector <pcl::PointIndices> &planes)
{
	//calculate features;
	concurrent_vector<featureCalculation::eigenFeature> ptFeatures(pointCloud.size());
	calculateEigenFeaturesBasedOnRadiusSearch(pointCloud, radiusFeature, ptFeatures);

	//根据点的曲率升序排序;
	sort(ptFeatures.begin(), ptFeatures.end(), CmpPointCurvatureS);

	set<size_t, less<size_t>>unSegment;
	set<size_t, less<size_t>>::iterator iterUnseg;
	pointCloudXYZI inputCloud;
	vector<bool> hasSeg;
	for (size_t i = 0; i < ptFeatures.size(); ++i)
	{
		unSegment.insert(i);
		inputCloud.push_back(ptFeatures[i].pt);
		hasSeg.push_back(false);
	}

	/*建立KD树*/
	pcl::KdTreeFLANN<pcl::PointXYZI>kdtree;
	std::vector<int>pointIdxSearch;
	std::vector<float>pointDistance;
	kdtree.setInputCloud(inputCloud.makeShared());


	/*建立队列deque,用于存储一个分割区域的种子点;*/
	deque <size_t>seed;
	while (!unSegment.empty())
	{
		pcl::PointIndices plane;

		/*选取剩余点中平面拟合残差最小的最小点为种子点;*/
		iterUnseg = unSegment.begin();
		int minNum = *iterUnseg;

		/*将种子点压入plane和seed中,并从unSegment中删除;*/
		unSegment.erase(minNum);
		seed.push_back(minNum);
		plane.indices.push_back(ptFeatures[minNum].ptIndex);
		hasSeg[minNum] = true;
		
		/*得到最初种子点的法线nx,ny,nz;*/
		double nx, ny, nz;
		nx = ptFeatures[minNum].normalDirection.x();
		ny = ptFeatures[minNum].normalDirection.y();
		nz = ptFeatures[minNum].normalDirection.z();
		
		while (!seed.empty())
		{
			double a, b, c, d;
			size_t ptIndex = seed.front();
			seed.pop_front();//种子点弹出;
			/*得到当前种子点的平面方程;*/
			a = ptFeatures[ptIndex].normalDirection.x();
			b = ptFeatures[ptIndex].normalDirection.y();
			c = ptFeatures[ptIndex].normalDirection.z();
			d = ptFeatures[ptIndex].dis;
			/*寻找种子点的邻域点;*/
			int N = kdtree.radiusSearch(ptFeatures[ptIndex].pt, searchRadius, pointIdxSearch, pointDistance);
			if (N > 0)
			{
				for (size_t i = 0; i < N; ++i)
				{
					/*面状点生长;*/
					if (false == hasSeg[pointIdxSearch[i]])
					{
						/*得到当前点的法向量;*/
						float nx1 = ptFeatures[pointIdxSearch[i]].normalDirection.x();
						float ny1 = ptFeatures[pointIdxSearch[i]].normalDirection.y();
						float nz1 = ptFeatures[pointIdxSearch[i]].normalDirection.z();
						/*计算当前邻域点和初始种子点法向量的夹角余弦值;*/
						float Cosnormal = ComputeIncludedAngleBetweenVector(nx, ny, nz, nx1, ny1, nz1);
						/*计算邻域点到当前种子点所在平面的垂直距离;*/
						float dis = ComputeDistanceFromPointToPlane(ptFeatures[pointIdxSearch[i]].pt, a, b, c, d);

						if (Cosnormal > cos(includedAngle) && dis < Pt2PlaneDis)//面状点生长的条件;
						{
							unSegment.erase(pointIdxSearch[i]);
							seed.push_back(pointIdxSearch[i]);
							plane.indices.push_back(ptFeatures[pointIdxSearch[i]].ptIndex);
							hasSeg[pointIdxSearch[i]] = true;

						}
					}
				}
			}
			else
			{
				continue;
			}

		}
		if (plane.indices.size() > 1600)
		{
			planes.push_back(plane);
		}
		plane.indices.clear();
		seed.clear();
	}

	std::vector<int>().swap(pointIdxSearch);
	std::vector<float>().swap(pointDistance);
	inputCloud.swap(pointCloudXYZI());

	outputPlanes("plans.las", pointCloud, planes);
	return planes.size();
}

void planeSegmentation::outputPlanes(const std::string &fileName, pointCloudXYZI &pointCloud, const std::vector <pcl::PointIndices> &planes)
{

	dataIo::pointCloudBound bound;
	dataIo io;
	io.getCloudBound(pointCloud, bound);

	srand((unsigned)time(NULL));
	int R, G, B;
	int pt_num = 0;

	for (int i = 0; i < planes.size(); i++)
	{
		pt_num += planes[i].indices.size();
	}

	std::ofstream ofs;
	ofs.open(fileName, std::ios::out | std::ios::binary);

	if (ofs.is_open())
	{
		liblas::Header header;
		header.SetDataFormatId(liblas::ePointFormat2);
		header.SetVersionMajor(1);
		header.SetVersionMinor(2);
		header.SetMin(bound.minx, bound.miny, bound.minz);
		header.SetMax(bound.maxx, bound.maxy, bound.maxz);
		header.SetOffset((bound.minx + bound.maxx) / 2.0, (bound.miny + bound.maxy) / 2.0, (bound.minz + bound.maxz) / 2.0);
		header.SetScale(0.000001, 0.000001, 0.0001);
		header.SetPointRecordsCount(pt_num);
		liblas::Writer writer(ofs, header);
		liblas::Point pt(&header);
		for (int i = 0; i < planes.size(); i++)
		{
			R = rand() % 255;
			G = rand() % 255;
			B = rand() % 255;
			for (int j = 0; j < planes[i].indices.size(); j++)
			{
				pt.SetCoordinates(double(pointCloud.points[planes[i].indices[j]].x),
					              double(pointCloud.points[planes[i].indices[j]].y),
								  double(pointCloud.points[planes[i].indices[j]].z));
				pt.SetIntensity(10);
				pt.SetColor(liblas::Color(R, G, B));
				writer.WritePoint(pt);
			}
		}
		ofs.flush();
		ofs.close();
	}
}

void planeSegmentation::calculateTheFunctionOfPlane(pointCloudXYZI &planePointCloud, planeFunction &planeFun)
{

	CvMat* pData = cvCreateMat(planePointCloud.points.size(), 3, CV_32FC1);
	CvMat* pMean = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVals = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVecs = cvCreateMat(3, 3, CV_32FC1);

	for (size_t j = 0; j < planePointCloud.points.size(); ++j)
	{
		cvmSet(pData, j, 0, planePointCloud.points[j].x);
		cvmSet(pData, j, 1, planePointCloud.points[j].y);
		cvmSet(pData, j, 2, planePointCloud.points[j].z);
	}
	cvCalcPCA(pData, pMean, pEigVals, pEigVecs, CV_PCA_DATA_AS_ROW);

	//eigne vectors;
	planeFun.a = cvmGet(pEigVecs, 2, 0);
	planeFun.b = cvmGet(pEigVecs, 2, 1);
	planeFun.c = cvmGet(pEigVecs, 2, 2);

	//distance from original to plane;
	double a, b, c, d;
	double x0, y0, z0;
	a = planeFun.a;
	b = planeFun.b;
	c = planeFun.c;
	x0 = cvmGet(pMean, 0, 0);
	y0 = cvmGet(pMean, 0, 1);
	z0 = cvmGet(pMean, 0, 2);
	planeFun.d = -(a*x0 + b*y0 + c*z0);

	cvReleaseMat(&pEigVecs);
	cvReleaseMat(&pEigVals);
	cvReleaseMat(&pMean);
	cvReleaseMat(&pData);

}

void planeSegmentation::calculateTheBoundOfPlane(pointCloudXYZI &planePointCloud, planeFunction planeFun, planeBoxBound3d  &planeBound)
{
	//计算属于平面的点云在平面上的投影点;
	pointCloudXYZ projectionPointCloud3d;
	calculateProjectionPointCloud(planePointCloud, planeFun, projectionPointCloud3d);

	//计算projectionPointCloud3d的二维投影点;
	pointCloudXY projectionPointCloud2d;
	Projecion3DPlaneTo2D(planeFun, projectionPointCloud3d, projectionPointCloud2d);

	//计算平面的主方向;
	Eigen::Vector2f  principleDirection;
	computePrincipleDirectionOfPointCloud2d(projectionPointCloud2d, principleDirection);

	//将点云转换到PCA坐标系下;
	pointCloudXY transformPointCloud2d;
	transformPointCloudXaxisToPrincipleDirection(projectionPointCloud2d, principleDirection, transformPointCloud2d);

	//计算PCA坐标系下的4个角点;
	planeBoxBound2d bound2dInPCA;
	calculateBoundInPcaCoordinate(transformPointCloud2d, bound2dInPCA);

	//计算PCA坐标系下的4个角点在原坐标系下的坐标;
	planeBoxBound2d bound2dInOri;
	InverseTransformToOriginalCoordinate(bound2dInPCA, principleDirection, bound2dInOri);

	//把2d的角点返还回3d;
	Projecion2DPointTo3D(bound2dInOri, planeFun, planeBound);
}

void planeSegmentation::calculateProjectionPointCloud(pointCloudXYZI &planePointCloud, planeFunction planeFun, pointCloudXYZ &projectionPointCloud)
{
	//计算属于平面的点在平面上的投影点坐标;
	Plane_3 plane(planeFun.a, planeFun.b, planeFun.c, planeFun.d);

	for (size_t i = 0; i < planePointCloud.points.size(); ++i)
	{
		Point_3 pt(planePointCloud.points[i].x, planePointCloud.points[i].y, planePointCloud.points[i].z);
		Point_3 projectionPt;
		projectionPt = plane.projection(pt);
		pcl::PointXYZ pclPt;
		pclPt.x = projectionPt.x();
		pclPt.y = projectionPt.y();
		pclPt.z = projectionPt.z();
		projectionPointCloud.push_back(pclPt);
	}

}

void planeSegmentation::Projecion3DPlaneTo2D(planeFunction planeFun, pointCloudXYZ &projectionPointCloud3d, pointCloudXY &projectionPointCloud2d)
{
	if (abs(planeFun.c) <= 0.1) //认为是与XOY平面垂直  
	{
		if (abs(planeFun.b) <= 0.1) //往YOZ 投影
		{
			for (size_t i = 0; i < projectionPointCloud3d.size(); ++i)
			{
				pcl::PointXY pt;
				pt.x = projectionPointCloud3d.points[i].y;
				pt.y = projectionPointCloud3d.points[i].z;
				projectionPointCloud2d.push_back(pt);
			}
		}
		else //往XOZ 投影 
		{
			for (size_t i = 0; i < projectionPointCloud3d.size(); ++i)
			{
				pcl::PointXY pt;
				pt.x = projectionPointCloud3d.points[i].x;
				pt.y = projectionPointCloud3d.points[i].z;
				projectionPointCloud2d.push_back(pt);
			}
		}
	}
	else  //不与XOY垂直  则往XOY投影
	{
		for (size_t i = 0; i < projectionPointCloud3d.size(); ++i)
		{
			pcl::PointXY pt;
			pt.x = projectionPointCloud3d.points[i].x;
			pt.y = projectionPointCloud3d.points[i].y;
			projectionPointCloud2d.push_back(pt);
		}
	}
}

void planeSegmentation::calculateTheFeatureOfPlanes(pointCloudXYZI &pointCloud, const vector<pcl::PointIndices> &planesIndices, vector<planeFeature> &planesFeatures)
{
	//获得属于平面的点云;
	for (size_t i = 0; i < planesIndices.size(); ++i)
	{
		planeFeature planeFea;
		pointCloudXYZI planePointCloud;
		for (size_t j = 0; j < planesIndices[i].indices.size(); ++j)
		{
			planePointCloud.points.push_back(pointCloud.points[planesIndices[i].indices[j]]);
		}
		
		planeFunction planeFun;
		calculateTheFunctionOfPlane(planePointCloud, planeFun);
		planeBoxBound3d  planeBound;
		calculateTheBoundOfPlane(planePointCloud, planeFun, planeBound);
		planeFea.fun = planeFun;
		planeFea.bound = planeBound;
		planesFeatures.push_back(planeFea);

		//displayPlaneAndBound(planePointCloud, planeFea);

		planePointCloud.points.clear();

	}
	displayPlanesAndBounds(pointCloud, planesIndices, planesFeatures);

	ofstream ofs("planes.txt");
	ofs << planesFeatures.size() << endl;
	for (size_t i = 0; i < planesFeatures.size(); ++i)
	{
		ofs << setiosflags(ios::fixed) << setprecision(3) << planesFeatures[i].fun.a << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planesFeatures[i].fun.b << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planesFeatures[i].fun.c << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planesFeatures[i].fun.d << "  ";

		for (int j = 0; j < 4; ++j)
		{
			ofs << setiosflags(ios::fixed) << setprecision(3) << planesFeatures[i].bound.pt[j].x << "  "
				<< setiosflags(ios::fixed) << setprecision(3) << planesFeatures[i].bound.pt[j].y << "  "
				<< setiosflags(ios::fixed) << setprecision(3) << planesFeatures[i].bound.pt[j].z << "  ";
		}
		ofs << endl;
	}
	ofs.close();
}

void planeSegmentation::Projecion2DPointTo3D(planeBoxBound2d bound2d, planeFunction planeFun, planeBoxBound3d &bound3d)
{
	for (int i = 0; i < 4;i++)
	{
		if (abs(planeFun.c) <= 0.1) //认为是与XOY平面垂直  
		{
			if (abs(planeFun.b) <= 0.1) //往YOZ 投影
			{
				float x, y, z;
				y = bound2d.pt[i].x;
				z = bound2d.pt[i].y;

				if (planeFun.a != 0)
				{
					x = -(planeFun.b*y + planeFun.c*z + planeFun.d) / planeFun.a;
				}
				else
				{
					x = 0.0f;
				}
				bound3d.pt[i].x = x;
				bound3d.pt[i].y = y;
				bound3d.pt[i].z = z;
			}

			else //往XOZ 投影 
			{
				float x, y, z;
				x = bound2d.pt[i].x;
				z = bound2d.pt[i].y;

				if (planeFun.b != 0)
				{
					y = -(planeFun.a*x + planeFun.c*z + planeFun.d) / planeFun.b;
				}
				else
				{
					y = 0.0f;
				}
				bound3d.pt[i].x = x;
				bound3d.pt[i].y = y;
				bound3d.pt[i].z = z;

			}
		}
		else  //不与XOY垂直  则往XOY投影
		{
			float x, y, z;
			x = bound2d.pt[i].x;
			y = bound2d.pt[i].y;

			if (planeFun.c != 0)
			{
				z = -(planeFun.a*x + planeFun.b*y + planeFun.d) / planeFun.c;
			}
			else
			{
				z = 0.0f;
			}
			bound3d.pt[i].x = x;
			bound3d.pt[i].y = y;
			bound3d.pt[i].z = z;
		}
	}
}


void planeSegmentation::displayPlanesAndBounds(pointCloudXYZI &pointCloud, const std::vector<pcl::PointIndices> &planesIndices,
	                                           std::vector<planeFeature> &planesFeatures)
{
	srand((unsigned)time(NULL));
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB(new pcl::PointCloud<pcl::PointXYZRGB>);

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("planes"));
	viewer->setBackgroundColor(255, 255, 255);

	for (size_t j = 0; j < planesIndices.size(); ++j)
	{
		int R, G, B;
		R = rand() % 255;
		G = rand() % 255;
		B = rand() % 255;

		for (size_t i = 0; i < planesIndices[j].indices.size(); ++i)
		{
			pcl::PointXYZRGB TempPoint;
			TempPoint.x = pointCloud.points[planesIndices[j].indices[i]].x;
			TempPoint.y = pointCloud.points[planesIndices[j].indices[i]].y;
			TempPoint.z = pointCloud.points[planesIndices[j].indices[i]].z;
			TempPoint.r = R;
			TempPoint.g = G;
			TempPoint.b = B;
			cloudRGB->points.push_back(TempPoint);
		}
	}
	viewer->addPointCloud(cloudRGB);


	int n = 0;
	char t[256];
	string s;

	for (size_t i = 0; i < planesFeatures.size(); ++i)
	{
		pcl::PointXYZ pt1, pt2;
		for (int j = 0; j < 3; j++)
		{
			pt1.x = planesFeatures[i].bound.pt[j].x;
			pt1.y = planesFeatures[i].bound.pt[j].y;
			pt1.z = planesFeatures[i].bound.pt[j].z;

			pt2.x = planesFeatures[i].bound.pt[j + 1].x;
			pt2.y = planesFeatures[i].bound.pt[j + 1].y;
			pt2.z = planesFeatures[i].bound.pt[j + 1].z;

			sprintf(t, "%d", n);
			n++;
			s = t;
			viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
			viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

			sprintf(t, "%d", n);
			n++;
			s = t;
			viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

		}

		pt1.x = planesFeatures[i].bound.pt[3].x;
		pt1.y = planesFeatures[i].bound.pt[3].y;
		pt1.z = planesFeatures[i].bound.pt[3].z;

		pt2.x = planesFeatures[i].bound.pt[0].x;
		pt2.y = planesFeatures[i].bound.pt[0].y;
		pt2.z = planesFeatures[i].bound.pt[0].z;

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
		viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);


	}

	while (!viewer->wasStopped())
	{
		viewer->spinOnce();
		boost::this_thread::sleep(boost::posix_time::microseconds(1000));
	}
}

void planeSegmentation::computePrincipleDirectionOfPointCloud2d(pointCloudXY &projectionPointCloud2d, Eigen::Vector2f & principleDirection)
{
	if (projectionPointCloud2d.points.size() < 5)
		return ;

	CvMat* pData = cvCreateMat(projectionPointCloud2d.points.size(), 2, CV_32FC1);
	CvMat* pMean = cvCreateMat(1, 2, CV_32FC1);
	CvMat* pEigVals = cvCreateMat(1, 2, CV_32FC1);
	CvMat* pEigVecs = cvCreateMat(2, 2, CV_32FC1);

	for (size_t i = 0; i < projectionPointCloud2d.points.size(); ++i)
	{
		cvmSet(pData, i, 0, projectionPointCloud2d.points[i].x);
		cvmSet(pData, i, 1, projectionPointCloud2d.points[i].y);
	}
	cvCalcPCA(pData, pMean, pEigVals, pEigVecs, CV_PCA_DATA_AS_ROW);

	//eigne vectors;
	principleDirection.x() = cvmGet(pEigVecs, 0, 0);
	principleDirection.y() = cvmGet(pEigVecs, 0, 1);

	cvReleaseMat(&pEigVecs);
	cvReleaseMat(&pEigVals);
	cvReleaseMat(&pMean);
	cvReleaseMat(&pData);
}

void planeSegmentation::transformPointCloudXaxisToPrincipleDirection(pointCloudXY &projectionPointCloud2d, Eigen::Vector2f  principleDirection,
	                                                                 pointCloudXY &transformPointCloud2d)
{

	float direct_x = principleDirection.x();
	float direct_y = principleDirection.y();


	float x_PCADirect = direct_x;
	float PCADirect_model = sqrt(direct_x*direct_x + direct_y*direct_y);
	//std::cout<<"pca_model: "<<PCADirect_model<<endl;
	float cos_alpha = x_PCADirect / PCADirect_model;
	float alpha = acos(fabs(cos_alpha));

	if (direct_x<0 && direct_y>0)
		alpha = 3.14159 - alpha;
	if (direct_x < 0 && direct_y<0)
		alpha = alpha + 3.14159;
	if (direct_x>0 && direct_y < 0)
		alpha = 6.283185 - alpha;

	for (size_t  i = 0; i < projectionPointCloud2d.points.size(); ++i)
	{
		pcl::PointXY pt;
		float x = projectionPointCloud2d.points[i].x;
		float y = projectionPointCloud2d.points[i].y;

		pt.x = x*cos(alpha) + y*sin(alpha);
		pt.y = -x*sin(alpha) + y*cos(alpha);

		transformPointCloud2d.push_back(pt);
	}
}

void planeSegmentation::calculateBoundInPcaCoordinate(pointCloudXY &transformPointCloud2d, planeBoxBound2d &bound2d)
{
	double min_x = transformPointCloud2d.points[0].x;
	double min_y = transformPointCloud2d.points[0].y;
	
	double max_x = transformPointCloud2d.points[0].x;
	double max_y = transformPointCloud2d.points[0].y;

	for (size_t i = 0; i<transformPointCloud2d.points.size(); ++i)
	{
		//获取边界
		if (min_x>transformPointCloud2d.points[i].x)
			min_x = transformPointCloud2d.points[i].x;
		if (min_y > transformPointCloud2d.points[i].y)
			min_y = transformPointCloud2d.points[i].y;
		
		if (max_x < transformPointCloud2d.points[i].x)
			max_x = transformPointCloud2d.points[i].x;
		if (max_y < transformPointCloud2d.points[i].y)
			max_y = transformPointCloud2d.points[i].y;
	}
	
	bound2d.pt[0].x = min_x;
	bound2d.pt[0].y = min_y;

	bound2d.pt[1].x = max_x;
	bound2d.pt[1].y = min_y;

	bound2d.pt[2].x = max_x;
	bound2d.pt[2].y = max_y;

	bound2d.pt[3].x = min_x;
	bound2d.pt[3].y = max_y;
}

void planeSegmentation::InverseTransformToOriginalCoordinate(const planeBoxBound2d bound2dInPCA, Eigen::Vector2f  principleDirection,
	                                                         planeBoxBound2d &bound2dInOri)
{

	float direct_x = principleDirection.x();
	float direct_y = principleDirection.y();


	float x_PCADirect = direct_x;
	float PCADirect_model = sqrt(direct_x*direct_x + direct_y*direct_y);
	//std::cout<<"pca_model: "<<PCADirect_model<<endl;
	float cos_alpha = x_PCADirect / PCADirect_model;
	float alpha = acos(fabs(cos_alpha));

	if (direct_x<0 && direct_y>0)
		alpha = 3.14159 - alpha;
	if (direct_x < 0 && direct_y<0)
		alpha = alpha + 3.14159;
	if (direct_x>0 && direct_y < 0)
		alpha = 6.283185 - alpha;


	for (int i = 0; i < 4; i++)
	{
		float x = bound2dInPCA.pt[i].x;
		float y = bound2dInPCA.pt[i].y;

		bound2dInOri.pt[i].x = x*cos(alpha) - y*sin(alpha);
		bound2dInOri.pt[i].y = x*sin(alpha) + y*cos(alpha);
	}
}

void planeSegmentation::displayPlaneAndBound(pointCloudXYZI &pointCloud, planeFeature planeFea)
{
	srand((unsigned)time(NULL));
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB(new pcl::PointCloud<pcl::PointXYZRGB>);

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("planes"));
	viewer->setBackgroundColor(255, 255, 255);

	int R, G, B;
	R = rand() % 255;
	G = rand() % 255;
	B = rand() % 255;

	for (size_t i = 0; i < pointCloud.size(); ++i)
	{
		pcl::PointXYZRGB TempPoint;
		TempPoint.x = pointCloud.points[i].x;
		TempPoint.y = pointCloud.points[i].y;
		TempPoint.z = pointCloud.points[i].z;
		TempPoint.r = R;
		TempPoint.g = G;
		TempPoint.b = B;
		cloudRGB->points.push_back(TempPoint);
	}
	viewer->addPointCloud(cloudRGB);


	int n = 0;
	char t[256];
	string s;


	pcl::PointXYZ pt1, pt2;
	for (int j = 0; j < 3; j++)
	{
		pt1.x = planeFea.bound.pt[j].x;
		pt1.y = planeFea.bound.pt[j].y;
		pt1.z = planeFea.bound.pt[j].z;

		pt2.x = planeFea.bound.pt[j + 1].x;
		pt2.y = planeFea.bound.pt[j + 1].y;
		pt2.z = planeFea.bound.pt[j + 1].z;

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
		viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

	}

	pt1.x = planeFea.bound.pt[3].x;
	pt1.y = planeFea.bound.pt[3].y;
	pt1.z = planeFea.bound.pt[3].z;

	pt2.x = planeFea.bound.pt[0].x;
	pt2.y = planeFea.bound.pt[0].y;
	pt2.z = planeFea.bound.pt[0].z;

	sprintf(t, "%d", n);
	n++;
	s = t;
	viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
	viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

	sprintf(t, "%d", n);
	n++;
	s = t;
	viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

	while (!viewer->wasStopped())
	{
		viewer->spinOnce();
		boost::this_thread::sleep(boost::posix_time::microseconds(1000));
	}
}
