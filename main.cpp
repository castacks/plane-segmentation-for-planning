#include "dataIo.h"
#include "VoxelFilter.h"
#include "featureCalculation.h"
#include "planeSegmentation.h"

#include <string>

using namespace  std;

int main()
{
	//Input Point Cloud;
	dataIo io;
	pointCloudXYZI pointCloud;
	string fileName;
	cout << "Please Input The FileName" << endl;
	cin >> fileName;
	io.readPointCloudFromLasFile(fileName, pointCloud);
	cout << "-----finish readPointCloud-----" << endl;
	//simplified Point Cloud;
	VoxelFilter<pcl::PointXYZI> filter(0.05);
	pcl::PointCloud<pcl::PointXYZI>::Ptr simplifiedPointCloud = filter.filter(pointCloud.makeShared());
	io.writePointCloudIntoLasFile("sim.las",*simplifiedPointCloud);

	//Plane Segmentation;
	float radiusFeature = 0.3f;
	float includedAngle = 15.0f*M_PI / 180.0f;
	float Pt2PlaneDis = 0.4f;
	float searchRadius = 1.0f;
	planeSegmentation ps;
	std::vector <pcl::PointIndices> planesIndices;
	vector<planeSegmentation::planeFeature> planesFeatures;
	ps.planeSegmentationBasedOnRegionGrowingRadius(*simplifiedPointCloud, radiusFeature, includedAngle, Pt2PlaneDis, radiusFeature, planesIndices);
	ps.calculateTheFeatureOfPlanes(*simplifiedPointCloud, planesIndices, planesFeatures);
}