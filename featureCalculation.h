
#ifndef FEATURECALCULATION
#define FEATURECALCULATION


#include "dataIo.h"

#include <vector>

#include <concurrent_vector.h>
#include <ppl.h>
#include <Eigen/src/Core/Matrix.h>

class featureCalculation:public dataIo
{
public:

	struct eigenFeature
	{
		Eigen::Vector3f normalDirection;
		Eigen::Vector3f principleDirection;
		unsigned short dimension;
		float lamada1;
		float lamada2;
		float lamada3;
		double curvature;
		double dis;//原点到平面的距离;
		pcl::PointXYZI pt;
		size_t ptIndex;
	};

	bool calculateEigenFeaturesBasedOnNearestKSearch(pointCloudXYZI &cloud, unsigned short ptNum, concurrency::concurrent_vector<eigenFeature> &ptFeatures);
	bool calculateEigenFeaturesBasedOnRadiusSearch(pointCloudXYZI &cloud, float radius, concurrency::concurrent_vector<eigenFeature> &ptFeatures);

	bool calculateEigenFeature(pointCloudXYZI &cloud, std::vector<int> &searchIndices, eigenFeature &ptFeature);
	

protected:
private:
};
#endif